# W07 Guestbook Example

A simple Personal website which consists a personal web page , bulls n cows game and the guest book using Node, Express, BootStrap, EJS 
Which will allow the user to add new entries in Guest Book
## How to use

Open a command window in your C:\Users\S531376\44563\A03Patlolla folder.

## Prerequisites
npm package should be installed.
Browesr to check the output.

##Installing
Run npm install to install all the dependencies in the package.json file.

## Run
Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

##Routing
Get "/" - display your default home page
Get "/about" - display your about page
Get "/contact" - display your contact page
Post "/contact" - submit a new contact message (send email)
Get "/guestbook" - display all guestbook entries
Get "/new-entry" - display a form for creating a new guestbook entry
Post "/new-entry" - submit a new entry (add to an array of entries)

```
> npm install
> node gbapp.js
```

## Access the aplication
Point your browser to `http://localhost:8081`. 

